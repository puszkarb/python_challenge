#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 22:08:26 2018

@author: puszek
"""

# Re_one_more_time
import requests
import re

site = requests.get("http://www.pythonchallenge.com/pc/def/equality.html").text
data = re.findall("<!--.*?-->", site, re.DOTALL)[-1]
found = re.findall('[^A-Z][A-Z]{3}([a-z])[A-Z]{3}[^A-Z]',data)

print(''.join(found))
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 20:04:13 2018

@author: puszek
"""

import requests
import html2text

url = "http://www.pythonchallenge.com/pc/def/ocr.html"

r = requests.get(url)
content = r.text
new_content =""
for symbol in content:
    if symbol not in "!@#$%^&*()-_=+][{}|\';/.,<>?:}]\n":
        new_content+= symbol
        


file = open('ocr.txt' , 'w')
file.write(content)
file.write("\n\n\n\n\n\n##############################\n\n\n\n\n\n\n")
file.write(new_content)
file.close()

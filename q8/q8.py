#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 17:52:14 2018

@author: puszek
"""

import requests
import re
import bz2
import ast

response = requests.get("http://www.pythonchallenge.com/pc/def/integrity.html")
print(response.text)
results = open('result.png', 'wb')

html_text = response.text

# in commented area at the very end of the site, some probably bits are able to be seen

coded_credentials = re.findall("'(.*?)'", html_text)
print(coded_credentials)

un = "'" + coded_credentials[0] + "'"
user_name = ast.literal_eval("b%s" % un)
user_name = bz2.decompress(user_name).decode()

pw = "'" + coded_credentials[1] + "'"
password = ast.literal_eval("b%s" % pw)
password = bz2.decompress(password).decode()

print(user_name)
print(password)


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 14:31:06 2018

@author: puszek
"""

import requests 
from requests.auth import HTTPBasicAuth
import re

q10_url = "http://www.pythonchallenge.com/pc/return/bull.html"
response = requests.get(q10_url, auth=HTTPBasicAuth('huge', 'file'))
print(response.text)

# so we click on the map - the cow itself and find out that there is a text file
file_sequence = open("sequence.txt", 'w')
file_sequence.write(requests.get("http://www.pythonchallenge.com/pc/return/sequence.txt", auth=HTTPBasicAuth('huge', 'file')).text)
file_sequence.close()

a = ["1", "11", "21", "1211", "111221", "312211" ]

"""
this looks as code named look-and-say :
    1
    one one -> "11"
    two one -> "21"
    one two one one -> "1211"
    one one one two two one -> "111221"
    three one two two one one 
    one three one one two two two one
"""
counter = 1;
new_word = ""
tmp =''
print(a[-1])
for i in range(30):
    for char in a[-1]:
        if(tmp == char):
            counter += 1
        elif(tmp):
            new_word += str(counter)
            new_word += str(tmp)
            counter = 1
        tmp = char;
    new_word += str(counter)
    new_word += str(tmp)
    print(new_word)
    a.append(new_word)
    new_word = ''
    tmp = ''
    counter = 1
    
print(len(a[30]))

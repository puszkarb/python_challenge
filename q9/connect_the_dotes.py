#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 12:40:29 2018

@author: puszek
"""

import requests
from requests.auth import HTTPBasicAuth
import re
import matplotlib.pyplot as plt

q9_url = "http://www.pythonchallenge.com/pc/return/good.html"
response = requests.get(q9_url, auth=HTTPBasicAuth('huge', 'file'))

print(response.text)

response_file = open("response.txt", 'w')
response_file.write(response.text)
response_file.close()

image_url = "http://www.pythonchallenge.com/pc/return/good.jpg"
image_content = requests.get(image_url)

points = re.findall('([0-9][0-9]*)',response.text)[2:]

points_numerics = [int(x) for x in points ]


X = [x for index, x in enumerate(points_numerics) if index % 2 == 0 ]
y = [y for index, y in enumerate(points_numerics) if index % 2 == 1 ]

x = X[:-1]
plt.plot(x, y)

# So from plot generated above it's just fairly easy to claim, that the next site is
# ../cow.html, then another tip is given: "it's a male". Proper answe - bull.html





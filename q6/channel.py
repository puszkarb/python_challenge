#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 18:21:16 2018

@author: puszek
"""

import requests
import zipfile
import re

response = requests.get("http://www.pythonchallenge.com/pc/def/channel.html")
site = response.text
print(site)
# as shown on the print, first line gives us clear instruction what to do.
zip_url = "http://www.pythonchallenge.com/pc/def/channel.zip"
zip_name = 'channel.zip'

print("Downloading zip...\t", end = "")
zip_response = requests.get(zip_url)
print("Finsihed.")
print("Packing zip content into an archive... ", end ="")
zip_file = open(zip_name, 'wb')
zip_file.write(zip_response.content)
zip_file.close()
print("Finished\n")

print("Reding the readme.txt file from the archive:")
archive = zipfile.ZipFile(zip_name, 'r')
readme_file = archive.read('readme.txt')
print(readme_file)
file_list = archive.filelist
#Ok so now we know that our starting point is 90052 file, let's proceed...

file_to_read = '90052.txt'
find_something = False
while find_something == False:
    file = archive.read(file_to_read).decode('utf-8')
    print( file)
    prefix = re.findall('Next nothing is ([0-9]*)', file)
    prefix = ''.join(prefix)
    if prefix != '':
        new_file_name = prefix + '.txt'
        print("Going into ---> "+ new_file_name)
        file_to_read = new_file_name
    else:
        find_something = True
        
#As we can see in the very last file tip assuming comment 
#is given, so let's explore this field by printing the loop again, 
#this time comments only
    
file_to_read = '90052.txt'   
find_something = False     
result_file = open("collected_comments.txt", 'wb')

while find_something == False:
    file = archive.read(file_to_read).decode('utf-8')
    prefix = re.findall('Next nothing is ([0-9]*)', file)
    prefix = ''.join(prefix)
    if prefix != '':
        new_file_name = prefix + '.txt'
        result_file.write( archive.getinfo(new_file_name).comment)
        file_to_read = new_file_name
    else:
        find_something = True
        result_file.close()
        archive.close()
print("\n\n\nCollected comments in file: " + result_file.name)
    


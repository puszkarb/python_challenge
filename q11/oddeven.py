be#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 20:18:39 2018

@author: puszek
"""

"""
odd even
"""

import requests
from requests.auth import HTTPBasicAuth
from PIL import Image
import numpy as np

response = requests.get("http://www.pythonchallenge.com/pc/return/5808.html", auth = HTTPBasicAuth("huge", "file"))
print(response.text)

"""
When we look at the page the only thing coming to mind is that the photo seen there is quite blured,
however, if we consider that this might be a fussion of two photos, were one of them is coded only on
odd pixels and other on even ones, we can see that this is quite possible idea.

"""

image = Image.open(requests.get("http://www.pythonchallenge.com/pc/return/cave.jpg", auth=HTTPBasicAuth("huge", "file"), stream = True).raw)
image.save("cage.jpg")
#print(image.size)

width = image.size[0]
height= image.size[1]
im1 = Image.new("RGB", (320, 240))
im2 = Image.new("RGB", (320, 240))

for i in range(height):
   for j in range(width):
       if j % 2 == 0 and i %2 == 0:
           im1.putpixel((int(j/2), int(i/2)), image.getpixel((j, i)))
       else:
           im2.putpixel((int(j/2), int(i/2)), image.getpixel((j, i)))
           

im1.save("im1.jpg")
im2.save("im2.jpg")

# so the next step is evil.html - visible on both im1 and im2
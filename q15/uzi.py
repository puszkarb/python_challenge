#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 01:11:52 2018

@author: puszek
"""

import datetime
import calendar 

import requests
response = requests.get("http://www.pythonchallenge.com/pc/return/uzi.html", auth=HTTPBasicAuth("huge","file"), stream = True)

print(response.text)

year = 1006
date = datetime.date(year, 1, 26)
print(date)
while year <= 1996 :
    print(date.year)
    if calendar.isleap(date.year):

        if date.weekday() == 0:
            print(date)
    
    year += 10
    date = datetime.date(year, 1, 26)
    
    
    # Answer is 1756-01-26 -> Mozart's birthday ( next url .../mozart.html)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 18:30:37 2018

@author: puszek
"""

# They said that it was in the air ...

import requests
from PIL import Image
import re

#Lets inspect the site itself, shall we ?
response = requests.get("http://www.pythonchallenge.com/pc/def/hockey.html")
print(response.text)

"""
Ok so this task was not very convenient...
First thought coming to mind is double 'o' in 'look'
and when we just quickly make some reasoning with 'air'
obvious answer is 'oxygen', and so it is!
Let's proceed to the oxygen.html site then
"""
response = requests.get("http://www.pythonchallenge.com/pc/def/oxygen.html")
print("\n\n\n"+response.text)

#It's time to get and decode the picture
image = requests.get("http://www.pythonchallenge.com/pc/def/oxygen.png")
save_image = open("oxygen.png",'wb')
save_image.write(image.content)
save_image.close()

image = Image.open('oxygen.png')

pi = image.load()

last_x, last_y = 0, 0
last_char = ''
print(image.size)
decoded = open("decoded.txt", 'w')
decoded_so_far = ''

for x in range(1, image.size[0]-2):
    for y in range(image.size[1]-2):
        if (pi[x,y] == pi[x + 1,y + 1]) and (pi[x,y] == pi[x + 2, y + 2]):
            if ( pi[x,y][0] >= 97 and pi[x,y][0] <=122 ) or pi[x,y][0] == 32:
                if pi[x - 1, y - 1] != pi[x, y]:
                    if chr(pi[x,y][0]) != last_char:
                        decoded.write(str(chr(pi[x,y][0])))
                        decoded_so_far += chr(pi[x,y][0])
                        last_x, last_y = x, y
                        last_char = chr(pi[x,y][0])

print(decoded_so_far)
# ok so now we can se that there is something ! 
#now we have position of last 's' in "is" so we should 
#inspect the following pixels
decoded_so_far = ''
x = last_x
while x < image.size[0]-1:
        if (pi[x,last_y] == pi[x + 1,last_y + 1]):
                if pi[x - 1, last_y - 1] != pi[x, y]:
                    decoded.write(str(chr(pi[x,last_y][0])))
                    decoded_so_far += chr(pi[x,last_y][0])
                    x += 6
        x += 1
print(decoded_so_far)
# well well well now it's coded while being coded, while being the image ! 
#That is imposterous!

#Lets decode the second code
coded_str = decoded_so_far[2:-1]
print(coded_str)
coded_str = coded_str.split(sep=', ')
decoded.write('\n')
for num in coded_str:
    decoded.write(chr(int(num)))
print(coded_str)
decoded.close()

#Ok so code is "integrity" - easy to find in decoded file

# other way 
row = [pi[x, image.size[1]/2 ] for x in range(image.size[0]) ]

row = row[::7]


single = [r for r,g,b,a in row if r ==  g == b]
single = ''.join(map(chr, single))  # <------ WORTH TO REMEMBER !

numbers = [int(x) for x in re.findall('\d+', single)]

result = ''.join(map(chr, numbers))
print("\n\nThe result is:   " + result)
image.close()
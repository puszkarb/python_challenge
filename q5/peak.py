#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 16:12:27 2018

@author: puszek
"""

### q5 - peek hell

import pickle
import requests
import re

site = requests.get("http://www.pythonchallenge.com/pc/def/peak.html").text
print(site)

file_url = "http://www.pythonchallenge.com/pc/def/"+ re.findall('<peakhell src="([a-z]*.[a-z])"/>', site)[-1]
print(file_url) 
banner = requests.get(file_url)
banner = banner.content
file = open("banner.p", 'wb')
file.write(banner)
file.close()

answer = open("answer.txt", 'w')
loaded_banner = pickle.load(open('banner.p', 'rb'))
for load in loaded_banner:
    for touple in load:
        answer.write(touple[0] * touple[1])
    answer.write("\n")
answer.close()

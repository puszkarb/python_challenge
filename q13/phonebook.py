#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 01:19:42 2018

@author: puszek
"""

import requests
from requests.auth import HTTPBasicAuth
from xml.etree import ElementTree
from xmlrpc.client import ServerProxy
response = requests.get("http://www.pythonchallenge.com/pc/phonebook.php", auth=HTTPBasicAuth("huge", "file"))

s = ServerProxy('http://www.pythonchallenge.com/pc/phonebook.php')

print(s.system.listMethods())

print(s.system.methodSignature("phone"))

print(s.system.methodHelp("phone"))
print(s.phone("evil"))

# is it bert ?
print(s.phone("Bert"))
print(s.phone("666"))


#555-ITALY? -> 555-44482555999 - 555 is a fkake number 

#Answer = ITALY


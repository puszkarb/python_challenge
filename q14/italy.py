#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 13:10:59 2018

@author: puszek
"""

import requests
from PIL import Image
from requests.auth import HTTPBasicAuth

im_response = requests.get("http://www.pythonchallenge.com/pc/return/wire.png", auth=HTTPBasicAuth("huge","file"), stream = True)
image = Image.open(im_response.raw)
new_image = Image.new("RGB", (100,100))


with open("file.txt", 'w') as f:
    for it in range(image.size[0]):
        f.writelines(str(image.getpixel((it,0))) +" " + str(it) + "\n")

x, y, pixel = -1, 0, 0
delta = [(1,0), (0,1), (-1, 0), (0, -1)]
edge_len = 200

while edge_len / 2 > 0:
    for vector in delta:
        steps = edge_len // 2
        for step in range(steps):
            x, y = x + vector[0], y + vector[1]
            new_image.putpixel((x,y), image.getpixel((pixel, 0)))
            pixel += 1
        edge_len -= 1
new_image.show()

        
        
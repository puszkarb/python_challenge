#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 19:17:38 2018

@author: puszek
"""

import string

string_my = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
result = "_my"
alphabet ='abcdefghijklmnopqrstuvwxyz'
for symbol in string_my:
    l = symbol
    if symbol in alphabet:
        l =(chr((ord(symbol)+2)))
        if ord(l) >= 123:
            l = chr(ord(l) - 26)
    result += l
    
print(result)

string_my = "pc/def/map.html"
result =''
for symbol in string_my:
    l = symbol
    if symbol in alphabet:
        l =(chr((ord(symbol)+2)))
        if ord(l) >= 123:
            l = chr(ord(l) - 26)
    result += l
    
print(result)
string_my = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."

table = string_my.maketrans(string.ascii_lowercase, string.ascii_lowercase[2:] + string.ascii_lowercase[:2])
print(string_my.translate(table))